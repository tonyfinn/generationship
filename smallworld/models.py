from kivy.event import EventDispatcher
from kivy.properties import NumericProperty, ObjectProperty, BooleanProperty, ListProperty, DictProperty
from kivy.logger import Logger

from enum import Enum
from collections import namedtuple, defaultdict, Counter
from typing import Tuple, NamedTuple, Mapping, List

import random
import math

class Resource(Enum):
    Energy = 1
    Food = 2
    AP = 3
    Speed = 4
    Housing = 5
    People = 6

class StructureType:
    def __init__(self, 
                 size: Tuple[int, int] = None, 
                 name: str = 'Empty', 
                 image: str = '', 
                 ap: int = 3, 
                 workers: int = 0,
                 bonuses: Mapping[Resource, int] = None,
                 text: str = 'Select a building on the right. Buildings cost AP to place, and produce resources. You get 1 AP per unused person'):
        if not size:
            size = (2, 2)
        if not bonuses:
            bonuses = {} # type: Mapping[Resource, int]
        self.size = size
        self.name = name
        self.image = image
        self.ap = ap
        self.bonuses = bonuses
        self.workers = workers
        self.text = text


class StructureTypes(Enum):
    Empty = StructureType(
        name='Empty', 
        size=(1, 1), 
        image='',
        bonuses={}
    )
    Farm = StructureType(
        name='Farm', 
        size=(4, 4), 
        image='assets/farm.png', 
        bonuses={Resource.Food: 5, Resource.Energy: -1},
        workers=1,
        ap=2,
        text='Farms produce food and allow you to gain more people. (Food +3, Energy -1, AP 2, Workers 1)'
    )
    Housing = StructureType(
        name='Housing', 
        size=(2, 2), 
        image='assets/housing.png', 
        bonuses={Resource.Housing: 4, Resource.Energy: -1},
        workers=0,
        text='Housing is required for your population. (Houses 2 people, Energy -1, AP 3)'
    )
    Energy = StructureType(
        name='Power', 
        size=(3, 3), 
        image='assets/power.png', 
        bonuses={Resource.Energy: 5},
        workers=2,
        text='Produces power to power other buildings. (Energy +3, AP 3, Workers 1)'
    )

class BuildMode(Enum):
    Highlight = StructureType(name='highlight', size=(1, 1), image='assets/highlight-tile.png', bonuses={})
    Farm = StructureTypes.Farm.value
    Housing = StructureTypes.Housing.value
    Energy = StructureTypes.Energy.value


class DisableStatus(Enum):
    Enabled = 0
    NoPeople = 1
    NoPower = 2
    Manual = 3


RectPosition = Tuple[Tuple[int, int], Tuple[int, int]]

def check_intersect(first_coord: RectPosition, second_coord: RectPosition) -> bool:
    first_start, first_end = first_coord
    other_start, other_end = second_coord
    first_start_x, first_start_y = first_start
    first_end_x, first_end_y = first_end
    other_start_x, other_start_y = other_start
    other_end_x, other_end_y = other_end
    if first_end_x < other_start_x or first_end_y < other_start_y:
        return False
    if other_end_x < first_start_x or other_end_y < first_start_y:
        return False
    
    return True


class Structure(EventDispatcher):
    unique_id = 0
    disabled = ObjectProperty()

    def __init__(self, name: str, structure_info: StructureType, start_pos: Tuple[int, int]) -> None:
        self.name = name
        self.structure_info = structure_info
        self.start_pos = start_pos
        self.id = self.unique_id
        self.unique_id += 1
        Logger.info('Structure({}, {}, {})'.format(name, structure_info, self.start_pos))
        self.disabled = DisableStatus.Enabled

    def intersects(self, other_structure: 'Structure') -> bool:
        return self._intersects(other_structure.start_pos, other_structure.end_pos)

    def intersects_potential(self, structure_type: StructureType, start_pos: Tuple) -> bool:
        startx, starty = start_pos
        width, height = structure_type.size
        potential_end_pos = startx + (width - 1), starty + (height - 1)
        return self._intersects(start_pos, potential_end_pos)

    def _intersects(self, start_pos: Tuple[int, int], end_pos: Tuple[int, int]) -> bool:
        return check_intersect((self.start_pos, self.end_pos), (start_pos, end_pos))

    @property
    def end_pos(self) -> Tuple[int, int]:
        startx, starty = self.start_pos
        width, height = self.structure_info.size
        return startx + (width - 1), starty + (height - 1)

    @property
    def required_workers(self):
        return self.structure_info.workers

    @property
    def energy_cost(self):
        return -self.structure_info.bonuses.get(Resource.Energy, 0)

    @property
    def produced_resources(self):
        return self.structure_info.bonuses


class NonBuildableZone(Structure):
    NON_BUILDABLE_ZONES = [
        ((0, 0), (7, 34)),
        ((0, 33), (68, 50)),
        ((60, 0), (68, 34)),
        ((0, 0), (26, 5)),
        ((48, 0), (68, 5)),
        ((0, 29), (26, 34)),
        ((48, 29), (48, 34)),
        ((6, 5), (13, 11)),
        ((6, 22), (13, 29))
    ]

    def __init__(self):
        super().__init__('', StructureTypes.Empty.value, (0, 0))

    def _intersects(self, start_pos, end_pos):
        return any(check_intersect(zone, (start_pos, end_pos)) for zone in self.NON_BUILDABLE_ZONES)
        

class Spaceship(EventDispatcher):
    structures = ListProperty()
    resources = DictProperty()

    def __init__(self, ship_width = (1024//15), ship_height = (512//15)):
        self.resources = {} # type: Mapping[Resource, int]
        self.resources[Resource.Food] = 0 # One unit = enough for 1 person for 1 turn
        self.resources[Resource.Energy] = 0 # Energy is expended to mvoe to the destination
                                             # 1 unit of energy over requirements = 1 progress 
        self.resources[Resource.Speed] = 10
        self.resources[Resource.People] = 3
        self.resources[Resource.Housing] = 0
        self.size = (ship_width, ship_height)
        self.tiles = {x: {y: None for y in range(0, ship_height)} for x in range(0, ship_width)}
        self.structures = [NonBuildableZone()] # type: List[Structure]

    def add_structure(self, structure: Structure) -> None:
        self.structures.append(structure)
        for resource, amount in structure.structure_info.bonuses.items():
            self.resources[resource] += amount
    
    def can_add_structure(self, structure_type: StructureType) -> bool:
        for resource, amount in structure_type.bonuses.items():
            if self.resources[resource] + amount < 0:
                return False
        return True

    def structure_at_tile(self, pos: Tuple[int, int]) -> Structure:
        relevant_structures = self.structures[1:]
        for structure in relevant_structures:
            if check_intersect((structure.start_pos, structure.end_pos), (pos, pos)):
                return structure

    @property
    def energy_stats(self):
        consumption = 0
        production = 0
        for structure in self.structures:
            structure_energy = structure.produced_resources.get(Resource.Energy, 0)
            if structure_energy > 0:
                production += structure_energy
            elif structure_energy < 0:
                Logger.debug('Using {} energy'.format(structure_energy))
                consumption -= structure_energy
        
        return (production, consumption)

    @property
    def net_energy(self):
        return self.resources[Resource.Energy]


class WinLossState(Enum):
    Active = 1
    Win = 2
    Loss = 3


class GameState(EventDispatcher):
    progress = NumericProperty()
    target = NumericProperty()
    current_turn = NumericProperty()
    ap = NumericProperty()
    build_mode = ObjectProperty()
    active_tile = ObjectProperty()
    active_structure = ObjectProperty(allownone=True)
    highlight_structure = ObjectProperty(allownone=True)
    is_intersect = BooleanProperty()
    win_loss = ObjectProperty()
    food_stockpile = NumericProperty()
    def __init__(self):
        self.build_mode = BuildMode.Highlight
        self.win_loss = WinLossState.Active
        self.active_structure = None
        self.highlight_structure = None
        self.spaceship = Spaceship() # type: Spaceship
        self.progress = 0 # type: int
        self.ap = 5
        self.target = 1600 # type: int
        self.current_turn = 0 # type: int
        self.active_tile = (0, 0)
        self.food_stockpile = 0

    @property
    def people(self):
        return self.spaceship.resources[Resource.People]

    @property
    def food(self):
        return self.spaceship.resources[Resource.Food]

    @people.setter
    def people(self, new_amount):
        self.spaceship.resources[Resource.People] = new_amount

    def process_turn(self):
        self.progress += self.spaceship.net_energy
        housing = self.spaceship.resources[Resource.Housing]
        
        grouped_structures = defaultdict(list)
        for structure in self.spaceship.structures:
            grouped_structures[structure.structure_info.name].append(structure)

        worked_structures = []
        worked_structures.extend(grouped_structures['Power'])
        worked_structures.extend(grouped_structures['Farm'])
        unworked_structures = grouped_structures['Housing']

        workers_left = self.people
        actual_resources = Counter() # type: Mapping[Resource, int]
        for structure in worked_structures:
            if structure.disabled == DisableStatus.Manual:
                continue
            if workers_left < structure.required_workers:
                structure.disabled = DisableStatus.NoPeople
            elif structure.energy_cost > actual_resources[Resource.Energy]:
                structure.disabled = DisableStatus.NoPower
            else:
                structure.disabled = DisableStatus.Enabled
                for resource, amount in structure.produced_resources.items():
                    actual_resources[resource] += amount
                    workers_left -= structure.required_workers


        net_food = actual_resources[Resource.Food] - self.people
        Logger.debug('Net food: {}'.format(net_food))
        if net_food >= 0:
            self.food_stockpile += net_food
            if self.people < housing:
                self.people = min(math.ceil(self.people * 1.2), housing)
        else:
            self.food_stockpile += 2*net_food
            if self.food_stockpile < 0:
                people_loss = max(1, int(self.people * 0.1))
                self.people -= people_loss


            
        self.current_turn += 1
        self.ap += (1 + max(workers_left, 0))
        if self.people <= 0:
            self.win_loss = WinLossState.Loss
        if self.progress >= self.target:
            self.win_loss = WinLossState.Win