import kivy
kivy.require('1.9.1')

from kivy.config import Config
Config.set('modules', 'inspector', '')

from kivy.app import App
from kivy.core.window import Window
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.floatlayout import FloatLayout

from smallworld.models import GameState, Spaceship, WinLossState
from smallworld.widgets import SpaceshipWidget, UIWidget

class GenerationShip(App):
    def build(self):
        self.layout = FloatLayout()
        self.start_game()
        return self.layout

    def start_game(self, *args):
        self.layout.clear_widgets()
        self.game_state = GameState()
        self.game_widget = SpaceshipWidget(state=self.game_state, size=(1024, 512), pos=(0, 256))
        self.layout.add_widget(self.game_widget)
        self.layout.add_widget(self.game_widget.dialog_layer)
        self.ui_widget = UIWidget(state=self.game_state, size=(1024, 256), size_hint=(None, None), pos=(0, 0))
        self.ui_widget.state = self.game_state
        self.layout.add_widget(self.ui_widget)
        self.game_state.bind(win_loss=self.display_end_screen)

    def display_end_screen(self, *args):
        win_text = 'You won in {} turns'.format(self.game_state.current_turn) if self.game_state.win_loss == WinLossState.Win else 'You ran out of food and lost'
        end_screen = AnchorLayout(anchor_x='center', anchor_y='center', size_hint=(1, 1))
        container = BoxLayout(height='75sp', orientation='vertical', width=200, size_hint=(None, None))
        self.layout.clear_widgets()
        label = Label(text=win_text, height='50sp', width=200)
        button = Button(text='Start Over', height='25sp', size_hint=(None, None), width=200)
        button.bind(on_press=self.start_game)
        container.add_widget(label)
        container.add_widget(button)
        end_screen.add_widget(container)
        self.layout.add_widget(end_screen)

if __name__ == '__main__':
    Window.size = (1024, 768)
    GenerationShip().run()