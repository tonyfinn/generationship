from typing import List

from kivy.core.window import Window
from kivy.graphics.context_instructions import Color
from kivy.graphics.instructions import InstructionGroup
from kivy.graphics.vertex_instructions import Rectangle, Line
from kivy.logger import Logger
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.relativelayout import RelativeLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.widget import Widget

from smallworld.models import GameState, BuildMode, Structure, Resource, DisableStatus
from smallworld.dialog import Dialog, DialogLayer

GRID_SIZE = 15
WIDTH = 1024
HEIGHT = 512
SHIP_OFFSET = 256


class StructureRenderer(InstructionGroup):
    def __init__(self, structure):
        super().__init__()
        self.disable_graphic = None
        self.structure = structure
        graphic = Rectangle(size=(GRID_SIZE * structure.structure_info.size[0], GRID_SIZE * structure.structure_info.size[1]),
                                source=structure.structure_info.image,
                                pos=(structure.start_pos[0] * GRID_SIZE, structure.start_pos[1] * GRID_SIZE))
        structure.bind(disabled=self.on_disabled_change)
        self.add(graphic)
        self.on_disabled_change()
    
    def on_disabled_change(self, *args):
        if self.disable_graphic:
            self.remove(self.disable_graphic)
        new_disable = None
        if self.structure.disabled == DisableStatus.NoPeople:
            new_disable = 'assets/nopeople.png'
        if self.structure.disabled == DisableStatus.NoPower:
            new_disable = 'assets/nopower.png'
        if self.structure.disabled == DisableStatus.Manual:
            new_disable = 'assets/manual-disable.png'
        if new_disable:
            self.disable_graphic = Rectangle(size=(20, 20), 
                                             source=new_disable,
                                             pos=(self.structure.start_pos[0] * GRID_SIZE, 
                                                  self.structure.start_pos[1] * GRID_SIZE))
            self.add(self.disable_graphic)
                            

def make_structure_label(struct):
    basic = '{}\nEnergy: {} / Food: {}\nWorkers: {}'.format(
        struct.name,
        struct.produced_resources.get(Resource.Energy, 0),
        struct.produced_resources.get(Resource.Food),
        struct.required_workers
    )
    if struct.disabled == DisableStatus.Manual:
        basic += '\n(disabled)'
    return basic


class SpaceshipWidget(RelativeLayout):

    def __init__(self, state: GameState, **kwargs) -> None:
        super().__init__(**kwargs)
        self.state = state
        self.grid = self.build_grid()
        self.placement_group = InstructionGroup()
        self.highlight_tile = Rectangle(size=(GRID_SIZE, GRID_SIZE), source='assets/grid-hover-cursor.png', pos=(0, 0))
        self.placement_group.add(self.highlight_tile)
        self.canvas.add(self.grid)
        self.canvas.add(self.placement_group)
        self.dialog_layer = DialogLayer()
        with self.canvas:
            self.structure_rects = InstructionGroup()
            spaceship_mask = Rectangle(size=(1024, 512), source='assets/spaceship-mask.png', pos=(0, 0))
            spaceship_graphic = Rectangle(size=(1024, 512), source='assets/spaceship.png', pos=(0, 0))
        self.state.bind(build_mode=self.update_build_mode)
        self.state.spaceship.bind(structures=self.add_structure_renderer)
        Window.bind(mouse_pos=self.on_mouse_move)

    def build_grid(self):
        grid = InstructionGroup()
        for x in range(0, WIDTH + GRID_SIZE, GRID_SIZE):
            grid.add(Line(points=[x, 0, x, HEIGHT]))
        for y in range(0, HEIGHT + GRID_SIZE, GRID_SIZE):
            grid.add(Line(points=[0, y, WIDTH, y]))
        return grid

    def add_structure_renderer(self, game_state: GameState, structures: List[Structure]) -> None:
        self.structure_rects.clear()
        for structure in structures:
            renderer = StructureRenderer(structure)
            self.structure_rects.add(renderer)

    def allow_build(self):
        return ((not self.is_build_blocked()) 
                and self.state.spaceship.can_add_structure(self.state.build_mode.value)
                and self.state.ap >= self.state.build_mode.value.ap)


    def make_structure_dialog(self, structure):
        layout = BoxLayout(orientation='vertical', size=(400, 200))
        label = Label(text=make_structure_label(structure))
        layout.add_widget(label)
        button = Button(text='Enable/disable')
        def toggle_structure_state(*args):
            if structure.disabled == DisableStatus.Manual:
                structure.disabled = DisableStatus.Enabled
            else:
                structure.disabled = DisableStatus.Manual
        button.bind(on_press=toggle_structure_state)
        layout.add_widget(button)
        dialog = Dialog(title=structure.name, content=layout)
        self.dialog_layer.add_widget(dialog)
    
    def update_build_mode(self, game_state: GameState, build_mode: BuildMode) -> None:
        old_pos = self.highlight_tile.pos
        structure_info = build_mode.value
        width, height = structure_info.size
        structure_size = (width * GRID_SIZE, height * GRID_SIZE)
        source = structure_info.image if self.allow_build() else 'assets/nobuild.png'
        self.highlight_tile = Rectangle(size=structure_size, source=source, pos=old_pos)
        self.placement_group.clear()
        if build_mode != BuildMode.Highlight:
            self.placement_group.add(Color(a=0.7))
        self.placement_group.add(self.highlight_tile)
        
        if build_mode != BuildMode.Highlight:
            self.placement_group.add(Color(a=1.0))

    def on_touch_down(self, touch):
        if 'button' in touch.profile and touch.button == 'left':
            gridx, gridy = self.get_tile_for_screen_pos(touch.pos, SHIP_OFFSET)
            if gridx == None:
                return
            if self.state.build_mode != BuildMode.Highlight and self.allow_build():
                structure_info = self.state.build_mode.value
                Logger.info(structure_info)
                self.state.spaceship.add_structure(
                    Structure(structure_info.name, structure_info, (gridx, gridy))
                )
                self.state.ap -= structure_info.ap
                self.state.build_mode = BuildMode.Highlight
                Logger.info('Built at {}'.format((gridx, gridy)))
            elif self.state.spaceship.structure_at_tile((gridx, gridy)):
                struct = self.state.spaceship.structure_at_tile((gridx, gridy))
                self.make_structure_dialog(struct)
    
    def is_build_blocked(self):
        return any(struct.intersects_potential(self.state.build_mode.value, self.state.active_tile) 
                for struct in self.state.spaceship.structures)

    def get_tile_for_screen_pos(self, value, offset):
        wx, wy = value
        x, y = wx, wy - offset
        if 0 < x < WIDTH and 0 < y < HEIGHT: 
            return x // GRID_SIZE, y // GRID_SIZE
        return (None, None)

    def on_mouse_move(self, prop, value):
        gridx, gridy = self.get_tile_for_screen_pos(value, SHIP_OFFSET)
        if gridx != None:
            self.state.active_tile = (int(gridx), int(gridy))
            self.highlight_tile.pos = (gridx * GRID_SIZE, (gridy * GRID_SIZE))
            self.highlight_tile.source = self.state.build_mode.value.image if self.allow_build() else 'assets/nobuild.png'

            if self.state.build_mode != BuildMode.Highlight:
                self.state.is_intersect = self.is_build_blocked()
            else:
                current_structure = self.state.spaceship.structure_at_tile(self.state.active_tile)
                self.state.highlight_structure = current_structure

class UIWidgetBox(BoxLayout):
    def __init__(self):
        super().__init__(orientation='vertical', size=(256, 256), size_hint=(None, None), padding=[10, 10, 10, 10])


class UIWidget(BoxLayout):
    def __init__(self, state, **kwargs):
        super().__init__(**kwargs)
        self.state = state
        self.size = (1024, 256)
        self.label_container = self.build_label_box()
        self.debug_panel = self.build_debug_box()
        self.turn_panel = self.build_turn_box()
        self.controls = self.build_controls_box()
        self.add_widget(self.label_container)
        self.add_widget(self.debug_panel)
        self.add_widget(self.turn_panel)
        self.add_widget(self.controls)
        with self.canvas.before:
            background = Rectangle(size=(1024, 256), source='assets/ui-background.png', pos=self.pos)
        self.init_from_state(state)

    def build_label_box(self):
        label_container = UIWidgetBox()
        self.progress_label = Label(text='Progress: 0/1600')
        self.people_label = Label(text='People')
        self.consumption_amount = Label(text='Consumption:\nEnergy: 0 / Food: 3', halign='center')
        self.production_amount = Label(text='Production:\nEnergy: 0 / Food: 0', halign='center')
        self.stockpile_amount = Label(text='Stockpile:\nFood: 0', halign='center')
        label_container.add_widget(self.progress_label)
        label_container.add_widget(self.people_label)
        label_container.add_widget(self.consumption_amount)
        label_container.add_widget(self.production_amount)
        label_container.add_widget(self.stockpile_amount)

        return label_container

    def build_debug_box(self):
        box = UIWidgetBox()
        info_label = Label(text='...', text_size=(200, 100), halign='left', valign='middle')
        status_label = Label(text='...', halign='center')
        def update_status_label(*args):
            if self.state.highlight_structure:
                status_label.text = make_structure_label(self.state.highlight_structure)
            else:
                status_label.text = '...'
        def update_build_mode(*args):
            info_label.text = self.state.build_mode.value.text
        self.state.bind(highlight_structure=update_status_label)
        self.state.bind(build_mode=update_build_mode)
        box.add_widget(status_label)
        box.add_widget(info_label)
        update_build_mode()
        return box

    def build_controls_box(self):
        controls = UIWidgetBox()
        self.build_farm_button = Button(text='Farm')
        self.build_energy_button = Button(text='Power')
        self.build_house_button = Button(text='Housing')
        controls.add_widget(self.build_farm_button)
        controls.add_widget(self.build_energy_button)
        controls.add_widget(self.build_house_button)
        self.build_farm_button.bind(on_press=self.build_farm)
        self.build_house_button.bind(on_press=self.build_house)
        self.build_energy_button.bind(on_press=self.build_energy)
        return controls

    def build_turn_box(self):
        box = UIWidgetBox()
        current_turn_label = Label(text='Current Turn: 0')
        ap_label = Label(text='AP')
        def update_ap_label(*args):
            ap_label.text = 'AP: {}'.format(self.state.ap)
        def update_turn_label(*args):
            current_turn_label.text = 'Current Turn: {}'.format(self.state.current_turn)
        self.state.bind(ap=update_ap_label, current_turn=update_turn_label)
        update_ap_label()
        self.end_turn_button = Button(text='End Turn')
        self.end_turn_button.bind(on_press=self.end_turn)
        box.add_widget(current_turn_label)
        box.add_widget(ap_label)
        box.add_widget(self.end_turn_button)
        return box

    def build_farm(self, button):
        self.state.build_mode = BuildMode.Farm

    def build_house(self, button):
        self.state.build_mode = BuildMode.Housing

    def build_energy(self, button):
        self.state.build_mode = BuildMode.Energy

    def end_turn(self, button):
        self.state.process_turn()

    def init_from_state(self, new_state: GameState) -> None:
        new_state.bind(progress=self.update_labels, target=self.update_labels)
        new_state.spaceship.bind(resources=self.update_labels, structures=self.update_labels)
        self.update_labels()

    def update_labels(self, changed_value=None, new_value=None):
        self.progress_label.text = 'Progress: {}/{}'.format(self.state.progress, self.state.target)
        self.production_amount.text = 'Production:\nEnergy: {} / Food: {}'.format(self.state.spaceship.energy_stats[0],
                                                                   self.state.spaceship.resources[Resource.Food])
        self.consumption_amount.text = 'Consumption:\nEnergy: {} / Food: {}'.format(
            self.state.spaceship.energy_stats[1],
            self.state.people
        )
        self.stockpile_amount.text = 'Stockpile:\nFood: {}'.format(self.state.food_stockpile)
        self.people_label.text = 'People: {}/{}'.format(
            self.state.spaceship.resources[Resource.People],
            self.state.spaceship.resources[Resource.Housing])