from kivy.uix.floatlayout import FloatLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.graphics.context_instructions import Color
from kivy.graphics.vertex_instructions import Rectangle


class DialogHeader(BoxLayout):

    def __init__(self, title, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.size_hint_x = 1.0
        self.height = 30
        self.label = Label(text=title, color=(0, 0, 0, 1))
        self.add_widget(self.label)
        self.close_button = Button(text='X')
        self.close_button.size = (30, 30)
        self.close_button.bind(pos=self.print_button_pos)
        self.close_button.size_hint = (None, None)
        self.close_button.bind(on_press=self.close)
        self.add_widget(self.close_button)

    def print_button_pos(self, button, pos):
        print('Button at {}'.format(pos))
        print('Button edges ({}, {})'.format(button.right, button.top))

    def close(self, button):
        print('Closing Dialog?')
        self.parent.close()


class Dialog(BoxLayout):
    def __init__(self, title, content = None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.size_hint = (0.5, None)
        self.orientation = 'vertical'
        self.pos_hint = {'center_x': 0.5, 'center_y': 0.5}
        self.add_widget(DialogHeader(title))
        if content:
            self.add_widget(content)
        with self.canvas.before:
            Color(1, 1, 1)
            self.bg_rect = Rectangle(size=self.size, pos=self.pos)

        self.bind(size=self.redraw, pos=self.redraw)

    def redraw(self, dialog, val):
        self.bg_rect.pos = self.pos
        self.bg_rect.size = self.size

    def close(self):
        self.parent.remove_widget(self)


class DialogLayer(FloatLayout):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def add_modal(self, modal):
        pass