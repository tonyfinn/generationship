from distutils.core import setup

setup(
    name='GenerationShip',
    version='0.1',
    packages=['smallworld'],
    long_description=open('README.md').read(),
    include_package_data=True
)