# Aim

Aiming for the Compo, might do Jam if I run out of time

# Technology Used

Python 3
Kivy - if UI heavy / Pyglet if lots of blitting sprites

# Technology TODOS before theme announcement
* Packaging
    - PyInstaller
    - py2exe/py2app



## Generation Ship
* A bit FTL-y with things breaking down
* Away missions?
* Less combat/roguelike, more survival game
* Turn based.
* Each turn is a year
* Random events to deal with each turn.
* Resource management
* Population control
* Some kind of tile based map to build on 


# Top 5

## You play as an atom
* Combine with other elemenets to make molecules?
* Fission/Fusion to make more elements?
* Lattice visuals for connections
* Goal = Make C02 (How to make further levels???)

## Civ in a cave?
* Send scouts out for food?
* Mine for space/resources
* Get attacked by adventurers from time to time?

## Needing to transport stuff off map?
* There's not enough space
* Amount of stuff expands over time
* You lose if the map fills up?
* Short term solutions (high rises?)
* Long term solutions (space program)

## Long sea voyage
* Keep people alive
* Resource management
* People demanding more than you actually have
    * People might fight or try overthrow the captain if they feel mistreated








# A small world
* Literally Small World (the board game)
    - Probably now
* Some setting that's pretty small
    - Civilization on Bender in the futurama episode?
        - May require art than I'm capable of
* start small and grow?
    - Use all resources in starting and expand?
    - fl0w, Spore cell stage?, Snake, Katamari
    - Atoms?
        - solar 2 but you need to combine to become heavier elements instead of larger systems?
        - Extra interactions?
            - Fission/Fusion somehow?
* Trading game but instead of going places, traders come to you, you're stuck in yoru small world
    - Patrician, Elite - combat
    - Upgrade your stuff
* You run the vendor for an rpg?
    - No dungeon crawling, not re-making recettear
* You run the tutorial town/rpg character training?
    - Does it fit theme?
* SimAnt
    - Loads of smaller agents doing goals
        - Time constraints?
* Something with world wrap? 
* Dungeon Keeper-esque

* Newspaper Tycoon
    - Making the world smaller?
    - Hard to get theme points

* Social circles
    - Redshirt-esque?
* Murder Mystery
    - Small town is co-operating to hide the culprit?
