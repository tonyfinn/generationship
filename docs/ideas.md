# Aim

Aiming for the Compo, might do Jam if I run out of time

# Technology Used

Python 3
Kivy - if UI heavy / Pyglet if lots of blitting sprites

# Technology TODOS before theme announcement
* Packaging
    - Bulldozer if Kivy
    - py2exe/py2app


## A small world
* Literally Small World
    - Probably not
* start small and grow?
    - Use all resources in starting and expand?
* Trading game but instead of going places, traders come to you, you're stuck in yoru small world
* You run the vendor for an rpg?
    - No dungeon crawling, not re-making recettear


# Theme Ideas

## You control the game, not the player
* Hero Manager
    - Dungeonland-ish
    - Setup the challenge
    - AI players would participate
* SimEarth/Spore ^-1
    - Set the conditions of the world
    - Different types of aliens (probably a hardcoded shortlist of options) would try build cities
    - Screw with them with god powers?
    - Goal is to keep them all even?

## Parallel Dimensions
* Something with two maps
    - Advance Wars: Dual Strike type switching
    - Send resources from one to the other

## Pausing has Consequences
* No idea yet, really don't want to come up
* Time messing about/rewinding/braid
    - Apply it to?

## Keep it Alive
* Some sort of table tennis-y idea to not drop an item
    - Make it more than pong
* Don't shoot the puppy 

## You are alone
* Last man standing defense
* Survival type game
    - How much can we achieve in 48 hours?

## Inconvenient Superpowers
* Platformer with randomly activated abilities
    - Or maybe if some bar charges up
    - And the abilities will impact your platforming
    - Laser eyes with knockback
    - Use them to reach high up platforms?
        - You'd need to to ensure you're in the right location
* Escort mission with aoe patterns that you need to avoid hitting the escorted npc?
    - 2d top down perspective

## Death is only the Beginning
* The paladin falls
* Bourne Situation
    - Spies maybe
    - Keep your cover story?
        - Text adventurey/Events
* Try to spread the zombie horde
    - Strategy esque
    - Board game with tiles
    - Actions you can spend to zombify tiles
    - Time limit before you get wiped out?

## Start with Nothing
* Idle game?
* Tower defense where you start having to click the enemies away
    - Eventually you'll get some towers
* Blank canvas
    - Viral Media tycoon
* Survival type games

## One tool, many uses
* Reconfigurable
* 0x10c super-lite?
    - Code to control spaceship?
    - How simple could we get?
    - Have written dcpu interpreter before

## Everything has a cost
* Some sort of card game maybe?
    - Limited resources
    - The order you play things
    - Find the best combination with your cards to maximise some other resource?
* Spying board game
    - Take turns against AI players
    - Each card represents an intel action
    - anything too aggresive increases a doomsday timer
    - If it gets too high, then war is declared and everyone uses




## Island
## A world in the skies
* Trading game?
    - A bit like patrician or similar
    - Go from one island or floating island to another with resources, trying ot make a proift

## On/Off
## Dark/Light
## Two Colors
* Binary logic thingy?
* Build an adder?
* Cellular automata game
    - Conway's Game of Life but with goals


## Dark/Light
* Control contents of map
    - One player places light sources
    - The other places walls
    - Some sort of ray tracing to light/ block out the map
    - Winner is whoever has most of the map?
    - Possibly hard to balance