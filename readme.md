# Generation Ship

# Aim

Aiming for the Compo, might do Jam if I run out of time

# Technology Used

Python 3
Kivy - if UI heavy / Pyglet if lots of blitting sprites

# Technology TODOS before theme announcement
* Packaging
    - PyInstaller
    - py2exe/py2app



## Generation Ship
* A bit FTL-y with things breaking down
* Away missions?
* Less combat/roguelike, more survival game
* Turn based.
* Each turn is a year
* Random events to deal with each turn.
* Resource management
* Population control
* Some kind of tile based map to build on 
* More people = more action points per turn, but more food used